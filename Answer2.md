# CI/CD

I have to say I haven't done much continuous integration programming but I will do my best to describe the process:

- speciffic tests are developed for the new features or bug fixes that will be introduced (this is TDD and could be skipped if test driven development is not being used).

- the developers work on a solution for the specific feature or bug fix and once they are satisfied with the solution and function specific tests have been passed, make a PR to merge their development branch with the production branch

- at this point the following process is automatically triggered, could be through Github Actions or a specific software like Jenkins; the new pushed code is tested against a large number of tests that are not specific to the new functionality but are rather standard for every PR to the production branch and cover a large number of processes, this is to make sure that the recently modified code hasn't affected other areas of the program. (further tasks besides testing can be automated, for example formatting code to make sure it follows a standarized set of lint rules or scanning for possible security threats like exposing secret keys).

- if any of the tests fail the integration process is aborted and the developers are prompted to solve the issue.

- if all tests pass then the new code is fully integrated to production and if necessary built and deployed.

---
