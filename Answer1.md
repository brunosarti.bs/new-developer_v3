# RTFM & LMGTFY

I actually had to LMGTFY that right before this 😅  
You caught me off guard

Actually nowadays I don't think it would be possible to work any other way, and luckily there is no need either.

I guess the largest googling and investigating I have done recently has been into Rust, I am learning some smart-contract development but mainly in Solidity which has a very JavaScript friendly look, and by a matter of chance ended up trying to write some very beginner smart contract for the Sui network which uses a Rust based language that's pretty new and not really very well documented. It was not easy and I can't say that I can manage Rust now, but it was interesting.

Now, as to operationg systems I use I will say that I code in a Windows PC every day though I don't really like windows command line so I use Bash, I was a Mac user for a very long time but wasn't programming back then; and finnaly even though I don't have Ubuntu installed on my every day PC I am familiar with it since most virtual machines and online services use it, besides... who doesn't like writting 'sudo'.

My programming languages of choice are definitely:

- JavaScript / TypeScript
- Python

As for others I have tried I would say that Java I found pretty confortable and Rust seemed pretty hard.

---
