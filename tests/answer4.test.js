const { findPairThatAddsTo } = require('../answer4');

describe('findPairThatAddsTo', () => {
  it('should return null if there are no elements in the array', () => {
    expect(findPairThatAddsTo([], 5)).toBeNull();
  });

  it('should return null if the last number in the array is less than half of num', () => {
    expect(findPairThatAddsTo([1, 3, 3, 7], 16)).toBeNull();
  });

  it('should return null if current number is already larger than sum', () => {
    expect(findPairThatAddsTo([12, 15, 20, 27], 11)).toBeNull();
  });

  it('should return valid pair', () => {
    expect(findPairThatAddsTo([0, 3, 4, 6], 7)).toEqual([3, 4]);
    expect(findPairThatAddsTo([1, 3, 4, 6], 7)).toEqual([1, 6]);
    expect(findPairThatAddsTo([1, 3, 3, 7], 6)).toEqual([3, 3]);
    expect(findPairThatAddsTo([0, 2, 3, 4, 6], 3)).toEqual([0, 3]);
  });
});
