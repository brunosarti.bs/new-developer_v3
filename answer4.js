/*
    Assuming that numbers are positive
    (would still work with negatives but could be further optimized)
    Assuming that repeated numbers in the array are valid
    for example [1, 3, 3, 7] sum=6, valid answer=[3,3]
    if repeated numbers wheren't valid then first converting Array to Set would be better
*/
const findPairThatAddsTo = (arr, sum) => {
  // if there are no elements in the array: return null
  if (!arr.length) return null;

  // if last number in the array is less than half of num
  // second to last can only be as big as last so: return null
  if (arr[arr.length - 1] * 2 < sum) return null;

  for (let i = 0; i < arr.length - 1; i += 1) {
    // if current number is already larger than sum: return null
    if (arr[i] >= sum) return null;
    // eslint-disable-next-line no-plusplus
    for (let j = i + 1; j < arr.length; j += 1) {
      if ((arr[i] + arr[j]) === sum) {
        return [arr[i], arr[j]];
      }
      if (arr[j] >= sum) {
        break;
      }
    }
  }

  // if no pair is found: return null
  return null;
};

module.exports = { findPairThatAddsTo };
